#configuracao vpc e subnet
resource "aws_vpc" "vpc-main" {
  cidr_block           = "192.168.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "${var.nome}-vpc"
  }
}

#regra libera ssh, http, https e dns de entrada e saida de internet > vm
resource "aws_security_group" "libera-externo" {
  name        = "libera-externo"
  vpc_id      = aws_vpc.vpc-main.id
  description = "Permite acesso ssh, http, https, e dns nas instancias vindo da internet e libera a rede interna para comunicacao com a internet."

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress {
    from_port   = 53
    to_port     = 53
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["192.168.0.0/16"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.nome}-libera-ssh-http-https-dns"
  }

}

#criacao da subnet publica
resource "aws_subnet" "public-subnet-main-01" {
  vpc_id     = aws_vpc.vpc-main.id
  cidr_block = "192.168.0.0/24"

  tags = {
    Name = "${var.nome}-public-subnet-main-01"
  }
}

#criacao do internet gateway
resource "aws_internet_gateway" "main-igw" {
  vpc_id = aws_vpc.vpc-main.id

  tags = {
    Name = "${var.nome}-main-igw"
  }
}

#criacao da tabela de roteamento
resource "aws_route_table" "main_rt" {
  vpc_id = aws_vpc.vpc-main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main-igw.id
  }

  tags = {
    Name = "${var.nome}-main_rt"
  }
}

#criacao da rota default para Acesso a internet
resource "aws_route" "aws_route_table_main_to_internet" {
  route_table_id         = aws_route_table.main_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main-igw.id
}

#associacao da subnet privada com a tabela de roteamento
resource "aws_route_table_association" "main_pub_association" {
  subnet_id      = aws_subnet.public-subnet-main-01.id
  route_table_id = aws_route_table.main_rt.id
}

#instancias rancher server, deploy e dns ec2
resource "aws_instance" "vm-ec2" {
  count                       = 5
  ami                         = "ami-06db4d78cb1d3bbf9"
  instance_type               = "t3.medium"
  key_name                    = "${var.chave_ssh_aws}"
  subnet_id                   = aws_subnet.public-subnet-main-01.id
  vpc_security_group_ids      = [ aws_security_group.libera-externo.id ]
  associate_public_ip_address = true
  
  root_block_device {
  volume_size = 30
  volume_type = "gp2"
  encrypted   = false
  }

  tags = {
  Name = "vm-ec2-${count.index +1}"  
  }
}
