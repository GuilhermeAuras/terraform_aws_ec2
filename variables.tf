variable "nome" {
  description = "ambiente de teste"
  default = "curso"
}

variable "chave_ssh_aws" {
  description = "curso-key-pair.pem"
  default = "curso-key-pair"
}

variable "home" {
  description = "home do usuario do curso"
  default = "/home/curso/"
}
