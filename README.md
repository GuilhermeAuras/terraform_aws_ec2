<h4> Mude os nomes para lhe atender melhor, mude para sua chave ssh ambos no arquivo variables.tf.</h4>
<h4> Necessario o Aws Cli esteja configurando e funcionando com um usuário com permissão de criacao/Administrador na AWS.</h4>

<h4>Aplicar:</h4>
* terraform init<br>
* terraform validate<br>
* terraform plan <br>
* terraform apply<br>

<h4>Destruir:</h4>
* terraform destroy<br>

<h4>Dependencias:</h4>
* A chave curso-key-pair deve-ser criada no painel da AWS.<br>
* O usuario curso e o home do mesmo deve existir para o terraform consiga criar a infraestrutura.<br>
